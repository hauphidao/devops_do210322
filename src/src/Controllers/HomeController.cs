﻿using Microsoft.AspNetCore.Mvc;
using ServiceStack.Redis;
using System;
using System.Collections.Generic;

namespace src.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HomeController : ControllerBase
    {
        private readonly RedisManagerPool manager;
        public HomeController()
        {
            string redisHost = Environment.GetEnvironmentVariable("REDIS_HOST");
            manager = new RedisManagerPool(redisHost);
        }

        [HttpGet("GetDataRedis")]
        public ActionResult GetDataRedis()
        {
            using (var client = manager.GetClient())
            {
                List<int> data = client.Get<List<int>>("TestCuoiKhoaDevops");
                if (data != null)
                    return Ok(data);
                return Ok("Vui lòng insert thêm data");
            }
        }

        [HttpGet("InsertDataRedis/{number}")]
        public ActionResult InsertDataRedis(int number)
        {
            using (var client = manager.GetClient())
            {
                List<int> data = client.Get<List<int>>("TestCuoiKhoaDevops");
                if (data != null)
                {
                    data.Add(number);
                }
                else
                {
                    data = new List<int>();
                    data.Add(number);
                }

                client.Set("TestCuoiKhoaDevops", data);
                return Ok("Đã thêm data thành công");
            }
        }

        [HttpGet("CleanCacheRedis")]
        public ActionResult CleanCacheRedis()
        {
            using (var client = manager.GetClient())
            {
                bool data = client.Remove("TestCuoiKhoaDevops");
                return Ok("Đã clean data");
            }
        }
    }
}
